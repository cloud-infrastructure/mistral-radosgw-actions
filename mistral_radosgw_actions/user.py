from mistral_radosgw_actions import base


class List(base.RadosGWAction):
    def __init__(self, rgw_access=None, rgw_secret=None, action_region=None):
        super(List, self).__init__(rgw_access, rgw_secret, action_region)

    def run(self, context):
        return [u._object for u in self.get_rgw().get_users()]

    def test(self, context):
        return None


class Get(base.RadosGWAction):
    def __init__(self, uid, stats=False,
                 rgw_access=None, rgw_secret=None, action_region=None):
        """Get the user information.
        :param str uid: the user id
        :param bool stats: True to retrieve the stats of the user
        :returns radosgw.user.UserInfo: the user info
        :throws radosgw.exception.RadosGWAdminError: if an error occurs
        :see: http://ceph.com/docs/next/radosgw/adminops/#get-user-info
        """
        super(Get, self).__init__(rgw_access, rgw_secret, action_region)
        self.uid = uid
        self.stats = stats

    def run(self, context):
        return self.get_rgw().get_user(uid=self.uid,
                                       stats=self.stats)._object

    def test(self, context):
        return None


class Create(base.RadosGWAction):
    def __init__(self, uid, display_name, email=None, key_type='s3',
                 access_key=None, secret_key=None, generate_key=True,
                 user_caps="", max_buckets=None, suspended=None,
                 rgw_access=None, rgw_secret=None, action_region=None):
        """Creates a new user.
        :param str uid: the user id
        :param str display_name: the display name
        :param str email: the user email
        :param str key_type: the key_type 's3' or 'swift'
        :param str access_key: the access key
        :param str secret_key: the secret key
        :param bool generate_key: True to auto generate a new key pair
        :param str user_caps: the user caps
        :param int max_buckets: max bucket for the user
        :param bool suspended: to suspend a user
        :return radosgw.user.UserInfo: the created user
        :see: http://ceph.com/docs/next/radosgw/adminops/#create-user
        """
        super(Create, self).__init__(rgw_access, rgw_secret, action_region)
        self.uid = uid
        self.display_name = display_name
        self.email = email
        self.key_type = key_type
        self.access_key = access_key
        self.secret_key = secret_key
        self.generate_key = generate_key
        self.user_caps = user_caps
        self.max_buckets = max_buckets
        self.suspended = suspended

    def run(self, context):
        return self.get_rgw().create_user(
            uid=self.uid,
            display_name=self.display_name,
            email=self.email,
            key_type=self.key_type,
            access_key=self.access_key,
            secret_key=self.secret_key,
            generate_key=self.generate_key,
            user_caps=self.user_caps,
            max_buckets=self.max_buckets,
            suspended=self.suspended,
        )._object

    def test(self, context):
        return None


class Update(base.RadosGWAction):
    def __init__(self, uid, display_name=None, email=None, key_type=None,
                 access_key=None, secret_key=None, generate_key=False,
                 user_caps=None, max_buckets=None, suspended=None,
                 rgw_access=None, rgw_secret=None, action_region=None):
        """Update an existing user.
        :param str uid: the user id
        :param str display_name: the display name
        :param str email: the user email
        :param str key_type: the key_type 's3' or 'swift'
        :param str access_key: the access key
        :param str secret_key: the secret key
        :param bool generate_key: True to auto generate a new key pair
        :param str user_caps: the user caps
        :param int max_buckets: max bucket for the user
        :param bool suspended: to suspend a user
        :return radosgw.user.UserInfo: the updated user
        :see: http://ceph.com/docs/next/radosgw/adminops/#modify-user
        """
        super(Update, self).__init__(rgw_access, rgw_secret, action_region)
        self.uid = uid
        self.display_name = display_name
        self.email = email
        self.key_type = key_type
        self.access_key = access_key
        self.secret_key = secret_key
        self.generate_key = generate_key
        self.user_caps = user_caps
        self.max_buckets = max_buckets
        self.suspended = suspended

    def run(self, context):
        return self.get_rgw().update_user(
            uid=self.uid,
            display_name=self.display_name,
            email=self.email,
            key_type=self.key_type,
            access_key=self.access_key,
            secret_key=self.secret_key,
            generate_key=self.generate_key,
            user_caps=self.user_caps,
            max_buckets=self.max_buckets,
            suspended=self.suspended,
        )._object

    def test(self, context):
        return None


class Delete(base.RadosGWAction):
    def __init__(self, uid, purge_data,
                 rgw_access=None, rgw_secret=None, action_region=None):
        """Delete a user identified by uid.
        :param str uid: the user_id
        :param bool purge_data: purge the user data
        :returns bool:
        :see: http://ceph.com/docs/next/radosgw/adminops/#remove-user
        """
        super(Delete, self).__init__(rgw_access, rgw_secret, action_region)
        self.uid = uid
        self.purge_data = purge_data

    def run(self, context):
        return self.get_rgw().delete_user(uid=self.uid,
                                          purge_data=self.purge_data)

    def test(self, context):
        return None


class CreateKey(base.RadosGWAction):
    def __init__(self, uid, key_type='s3', access_key=None, secret_key=None,
                 generate_key=True, rgw_access=None, rgw_secret=None,
                 action_region=None):
        """Creates a key for the user specified
        :param str uid: the user id
        :param str key_type: the key_type 's3' or 'swift'. Default: 's3'
        :param str access_key: the access key
        :param str secret_key: the secret key
        :param bool generate_key: True to auto generate a new key pair.
        :return bool:
        :see: http://docs.ceph.com/docs/master/radosgw/adminops/#create-key
        """
        super(CreateKey, self).__init__(rgw_access, rgw_secret, action_region)
        self.uid = uid
        self.key_type = key_type
        self.access_key = access_key
        self.secret_key = secret_key
        self.generate_key = generate_key

    def run(self, context):
        return self.get_rgw().create_key(
            uid=self.uid,
            key_type=self.key_type,
            access_key=self.access_key,
            secret_key=self.secret_key,
            generate_key=self.generate_key,
        )

    def test(self, context):
        return None


class RemoveKey(base.RadosGWAction):
    def __init__(self, access_key, uid=None, key_type='s3',
                 rgw_access=None, rgw_secret=None, action_region=None):
        """Delete an existing access key
        :param str access_key: the access key
        :param str uid: the user id
        :param str key_type: the key_type 's3' or 'swift'. Default: 's3'
        :returns bool:
        :see: http://docs.ceph.com/docs/master/radosgw/adminops/#remove-key
        """
        super(RemoveKey, self).__init__(rgw_access, rgw_secret, action_region)
        self.access_key = access_key
        self.uid = uid
        self.key_type = key_type

    def run(self, context):
        return self.get_rgw().remove_key(
            access_key=self.access_key,
            uid=self.uid,
            key_type=self.key_type)

    def test(self, context):
        return None
