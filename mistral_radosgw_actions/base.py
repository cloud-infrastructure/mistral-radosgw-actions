from oslo_config import cfg
from mistral_extra.actions.openstack.utils import keystone
from mistral_lib import actions

import radosgw

try:
    import urlparse as parse
except ImportError:
    from urllib import parse

CONF = cfg.CONF


class RadosGWAction(actions.Action):

    def __init__(self, rgw_access=None, rgw_secret=None, action_region=None):
        super(RadosGWAction, self).__init__()
        endpoint = self.get_service_endpoint(action_region)

        self.rgw_access = rgw_access
        self.rgw_secret = rgw_secret
        self.host, self.port = self._get_host_and_port(endpoint.url)

    def get_service_endpoint(self, action_region=None):
        """Get OpenStack service endpoint.

        'service_name' and 'service_type' are defined in specific OpenStack
        service action.
        """
        endpoint = keystone.get_endpoint_for_project(
            service_name='swift',
            service_type=None,
            region_name=action_region
        )

        return endpoint

    def get_rgw(self):
        return radosgw.connection.RadosGWAdminConnection(
            host=self.host,
            port=self.port,
            access_key=self.rgw_access,
            secret_key=self.rgw_secret,
            aws_signature=CONF.radosgw.aws_signature,
            is_secure=CONF.radosgw.is_secure)

    def _get_host_and_port(self, endpoint):
        url = parse.urlparse(endpoint)
        return (
            next(iter(url.netloc.split(':'))),
            url.port if url.port else 443 if url.scheme == 'https' else 80
        )

    def run(self, context):
        pass
