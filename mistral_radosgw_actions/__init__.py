from oslo_config import cfg

RADOSGW_GROUP = 'radosgw'

CONF = cfg.CONF

radosgw_opts = [
    cfg.StrOpt(
        'aws_signature',
        choices=['AWS2', 'AWS4'],
        default='AWS2',
        help='AWS signature to connect to radosgw AWS2 or AWS4'
    ),
    cfg.BoolOpt(
        'is_secure',
        default=True,
        help='Validate host CA certificate'
    )
]

CONF.register_opts(radosgw_opts, group=RADOSGW_GROUP)
