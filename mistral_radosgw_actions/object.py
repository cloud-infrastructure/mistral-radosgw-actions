from mistral_radosgw_actions import base


class Delete(base.RadosGWAction):
    def __init__(self, bucket_name, object_name,
                 rgw_access=None, rgw_secret=None, action_region=None):
        """Remove an existing object from a bucket.
        :param str bucket_name:
        :param str object_name:
        :param kwargs:
        :return: None
        """
        super(Delete, self).__init__(rgw_access, rgw_secret, action_region)
        self.bucket_name = bucket_name
        self.object_name = object_name

    def run(self, context):
        return self.get_rgw().remove_object(
            bucket_name=self.bucket_name,
            object_name=self.object_name)

    def test(self, context):
        return None
