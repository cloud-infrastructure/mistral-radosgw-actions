from mistral_radosgw_actions import base


class List(base.RadosGWAction):
    def __init__(self, uid=None,
                 rgw_access=None, rgw_secret=None, action_region=None):
        """Get all, or user specific, buckets information.
        :param str uid: the user id
        :returns list: the list of buckets information
        """
        super(List, self).__init__(rgw_access, rgw_secret, action_region)
        self.uid = uid

    def run(self, context):
        return [b._object for b in self.get_rgw().get_buckets(uid=self.uid)]

    def test(self, context):
        return None


class Get(base.RadosGWAction):
    def __init__(self, bucket_name, stats=True,
                 rgw_access=None, rgw_secret=None, action_region=None):
        """Get a bucket information.
        :param str bucket_name: the bucket name
        :returns BucketInfo:
        """
        super(Get, self).__init__(rgw_access, rgw_secret, action_region)
        self.bucket_name = bucket_name
        self.stats = stats

    def run(self, context):
        return self.get_rgw().get_bucket(
            bucket_name=self.bucket_name,
            stats=self.stats)._object

    def test(self, context):
        return None


class Check(base.RadosGWAction):
    def __init__(self, bucket_name, check_objects=True, fix=False,
                 rgw_access=None, rgw_secret=None, action_region=None):
        """Check the index of an existing bucket.
        :param str bucket_name:
        :param bool check_objects:
        :param bool fix:
        :return: nothing
        """
        super(Check, self).__init__(rgw_access, rgw_secret, action_region)
        self.bucket_name = bucket_name
        self.check_objects = check_objects
        self.fix = fix

    def run(self, context):
        return self.get_rgw().check_bucket_index(
            bucket_name=self.bucket_name,
            check_objects=self.check_objects,
            fix=self.fix
        )

    def test(self, context):
        return None


class Delete(base.RadosGWAction):
    def __init__(self, bucket_name, purge_objects=True,
                 rgw_access=None, rgw_secret=None, action_region=None):
        """Delete an existing bucket.
        :param str bucket_name:
        :param bool purge_objects:
        :return: None
        """
        super(Delete, self).__init__(rgw_access, rgw_secret, action_region)
        self.bucket_name = bucket_name
        self.purge_objects = purge_objects

    def run(self, context):
        return self.get_rgw().delete_bucket(
            bucket_name=self.bucket_name,
            purge_objects=self.purge_objects)

    def test(self, context):
        return None


class Unlink(base.RadosGWAction):
    def __init__(self, bucket_name, uid,
                 rgw_access=None, rgw_secret=None, action_region=None):
        """Unlink a bucket from a specified user.
        Primarily useful for changing bucket ownership.
        :param str bucket_name: name of the bucket to unlink
        :param str uid: current user id of the bucket
        :param kwargs:
        :return: None
        """
        super(Unlink, self).__init__(rgw_access, rgw_secret, action_region)
        self.bucket_name = bucket_name
        self.uid = uid

    def run(self, context):
        return self.get_rgw().unlink_bucket(
            bucket_name=self.bucket_name,
            uid=self.uid)

    def test(self, context):
        return None


class Link(base.RadosGWAction):
    def __init__(self, bucket_name, bucket_id, uid,
                 rgw_access=None, rgw_secret=None, action_region=None):
        """Link a bucket to a specified user, unlinking the bucket
        from any previous user.
        :param str bucket_name: name of the bucket to link
        :param str bucket_id: id of the bucket to link
        :param str uid: user ID to link the bucket to
        :param kwargs:
        :return: None
        """
        super(Link, self).__init__(rgw_access, rgw_secret, action_region)
        self.bucket_name = bucket_name
        self.bucket_id = bucket_id
        self.uid = uid

    def run(self, context):
        return self.get_rgw().link_bucket(
            bucket_name=self.bucket_name,
            bucket_id=self.bucket_id,
            uid=self.uid)

    def test(self, context):
        return None
