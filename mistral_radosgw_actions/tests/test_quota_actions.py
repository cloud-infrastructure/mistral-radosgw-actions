from mistral_radosgw_actions import quota as actions
from mistral_extra.actions.openstack.utils import keystone
from mistral_radosgw_actions.tests import EndpointHelper

from oslotest import base
from unittest import mock

QUOTA = {
    "enabled": False,
    "check_on_raw": False,
    "max_size": 1024,
    "max_size_kb": 1,
    "max_objects": 10
}


class QuotaActionTest(base.BaseTestCase):

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_quota_get_action(self, mocked_conn, mocked_ks):
        mocked_conn.return_value.get_quota.return_value = QUOTA
        mocked_ks.return_value = EndpointHelper('http://localhost')
        mock_ctx = mock.Mock()

        action = actions.Get(
            uid='fake',
            quota_type='user'
        )

        result = action.run(mock_ctx)

        mocked_conn.return_value.get_quota.assert_called_once_with(
            uid='fake',
            quota_type='user'
        )
        self.assertEqual(QUOTA, result)

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_quota_set_action(self, mocked_conn, mocked_ks):
        mocked_conn.return_value.set_quota.return_value = True
        mocked_ks.return_value = EndpointHelper('http://localhost')
        mock_ctx = mock.Mock()

        action = actions.Set(
            uid='fake',
            quota_type='user',
            max_objects=20,
            max_size_kb=2000,
            enabled=True
        )

        result = action.run(mock_ctx)

        mocked_conn.return_value.set_quota.assert_called_once_with(
            uid='fake',
            quota_type='user',
            max_objects=20,
            max_size_kb=2000,
            enabled=True
        )
        self.assertEqual(True, result)
