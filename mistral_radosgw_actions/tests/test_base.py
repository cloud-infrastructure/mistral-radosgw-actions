from mistral_radosgw_actions.base import RadosGWAction
from mistral_extra.actions.openstack.utils import keystone
from mistral_radosgw_actions.tests import EndpointHelper

from oslotest import base
from unittest import mock


class BaseTest(base.BaseTestCase):

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_base_http_no_port(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('http://localhost')

        obj = RadosGWAction(rgw_access='ACCESS',
                            rgw_secret='SECRET',
                            action_region='default')

        mocked_ks.assert_called_once_with(
            service_name='swift',
            region_name='default',
            service_type=None
        )

        self.assertEqual(obj.port, 80)
        self.assertEqual(obj.host, 'localhost')
        self.assertEqual(obj.rgw_access, 'ACCESS')
        self.assertEqual(obj.rgw_secret, 'SECRET')

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_base_https_no_port(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('https://localhost')

        obj = RadosGWAction(rgw_access='ACCESS',
                            rgw_secret='SECRET',
                            action_region='default')

        mocked_ks.assert_called_once_with(
            service_name='swift',
            region_name='default',
            service_type=None
        )

        self.assertEqual(obj.port, 443)
        self.assertEqual(obj.host, 'localhost')
        self.assertEqual(obj.rgw_access, 'ACCESS')
        self.assertEqual(obj.rgw_secret, 'SECRET')

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_base_http_with_port(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('http://localhost:8080')

        obj = RadosGWAction(rgw_access='ACCESS',
                            rgw_secret='SECRET',
                            action_region='default')

        mocked_ks.assert_called_once_with(
            service_name='swift',
            region_name='default',
            service_type=None
        )

        self.assertEqual(obj.port, 8080)
        self.assertEqual(obj.host, 'localhost')
        self.assertEqual(obj.rgw_access, 'ACCESS')
        self.assertEqual(obj.rgw_secret, 'SECRET')

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_base_https_with_port(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('https://localhost:8080')

        obj = RadosGWAction(rgw_access='ACCESS',
                            rgw_secret='SECRET',
                            action_region='default')

        mocked_ks.assert_called_once_with(
            service_name='swift',
            region_name='default',
            service_type=None
        )

        self.assertEqual(obj.port, 8080)
        self.assertEqual(obj.host, 'localhost')
        self.assertEqual(obj.rgw_access, 'ACCESS')
        self.assertEqual(obj.rgw_secret, 'SECRET')
