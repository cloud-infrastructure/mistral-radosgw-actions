from mistral_radosgw_actions import usage as actions
from mistral_extra.actions.openstack.utils import keystone
from mistral_radosgw_actions.tests import EndpointHelper

from oslotest import base
from unittest import mock


class UsageActionTest(base.BaseTestCase):

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_usage_get_action(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('http://localhost')
        mocked_conn.return_value.get_usage.return_value = {}
        mock_ctx = mock.Mock()

        action = actions.Get(
            uid='fake',
            start='2001-01-01 00:00:00.000000Z',
            end='2001-01-01 00:00:00.000000Z',
            show_summary=False,
            show_entries=False
        )

        result = action.run(mock_ctx)

        mocked_conn.return_value.get_usage.assert_called_once_with(
            uid='fake',
            start='2001-01-01 00:00:00.000000Z',
            end='2001-01-01 00:00:00.000000Z',
            show_summary=False,
            show_entries=False
        )
        self.assertIsInstance(result, dict)
        self.assertEqual({}, result)
