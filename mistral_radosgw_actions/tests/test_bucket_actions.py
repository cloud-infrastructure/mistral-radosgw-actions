from mistral_radosgw_actions import bucket as actions
from mistral_extra.actions.openstack.utils import keystone
from mistral_radosgw_actions.tests import EndpointHelper
from radosgw.bucket import BucketInfo

from oslotest import base
from unittest import mock


BUCKET = {
    "bucket": "example_bucket",
    "id": "79826.5",
    "index_pool": ".rgw.buckets",
    "marker": "79826.5",
    "master_ver": 0,
    "max_marker": "",
    "mtime": 0,
    "owner": "fake_user",
    "pool": ".rgw.buckets",
    "usage": {
        "rgw.main": {
            "num_objects": 1,
            "size_kb": 147,
            "size_kb_actual": 148
        }
    },
    "ver": 0
}


class BucketActionTest(base.BaseTestCase):

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_bucket_list_action(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('http://localhost')
        bucket = BucketInfo(radosgw_admin=mocked_conn,
                            bucket_dict=BUCKET)

        mocked_conn.return_value.get_buckets.return_value = [bucket]
        mock_ctx = mock.Mock()

        action = actions.List(
            uid='fake_user'
        )

        result = action.run(mock_ctx)

        mocked_conn.return_value.get_buckets.assert_called_once_with(
            uid='fake_user',
        )
        self.assertIn(bucket._object, result)

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_bucket_get_action(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('http://localhost')
        bucket = BucketInfo(radosgw_admin=mocked_conn,
                            bucket_dict=BUCKET)

        mocked_conn.return_value.get_bucket.return_value = bucket
        mock_ctx = mock.Mock()

        action = actions.Get(
            bucket_name=BUCKET['bucket'],
            stats=True
        )

        result = action.run(mock_ctx)

        mocked_conn.return_value.get_bucket.assert_called_once_with(
            bucket_name=BUCKET['bucket'],
            stats=True
        )
        self.assertEqual(BUCKET['bucket'], result['bucket'])

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_bucket_check_action(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('http://localhost')
        mock_ctx = mock.Mock()

        action = actions.Check(
            bucket_name=BUCKET['bucket'],
            check_objects=True,
            fix=False
        )

        action.run(mock_ctx)

        mocked_conn.return_value.check_bucket_index.assert_called_once_with(
            bucket_name=BUCKET['bucket'],
            check_objects=True,
            fix=False
        )

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_bucket_delete_action(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('http://localhost')
        mocked_conn.return_value.delete_bucket.return_value = True
        mock_ctx = mock.Mock()

        action = actions.Delete(
            bucket_name='fake_bucket',
            purge_objects=True
        )

        result = action.run(mock_ctx)

        mocked_conn.return_value.delete_bucket.assert_called_once_with(
            bucket_name='fake_bucket',
            purge_objects=True
        )
        self.assertEqual(True, result)

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_bucket_unlink_action(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('http://localhost')
        mocked_conn.return_value.unlink_bucket.return_value = True
        mock_ctx = mock.Mock()

        action = actions.Unlink(
            bucket_name='fake_bucket',
            uid='fake_user'
        )

        result = action.run(mock_ctx)

        mocked_conn.return_value.unlink_bucket.assert_called_once_with(
            bucket_name='fake_bucket',
            uid='fake_user'
        )
        self.assertEqual(True, result)

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_bucket_link_action(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('http://localhost')
        mocked_conn.return_value.link_bucket.return_value = True
        mock_ctx = mock.Mock()

        action = actions.Link(
            bucket_name='fake_bucket',
            bucket_id='fake_bucket_id',
            uid='fake_user'
        )

        result = action.run(mock_ctx)

        mocked_conn.return_value.link_bucket.assert_called_once_with(
            bucket_name='fake_bucket',
            bucket_id='fake_bucket_id',
            uid='fake_user'
        )
        self.assertEqual(True, result)
