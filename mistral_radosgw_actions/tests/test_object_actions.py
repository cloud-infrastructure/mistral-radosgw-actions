from mistral_radosgw_actions import object as actions
from mistral_extra.actions.openstack.utils import keystone
from mistral_radosgw_actions.tests import EndpointHelper

from oslotest import base
from unittest import mock


class ObjectActionTest(base.BaseTestCase):

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_object_delete_action(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('http://localhost')
        mocked_conn.return_value.remove_object.return_value = True
        mock_ctx = mock.Mock()

        action = actions.Delete(
            bucket_name='fake_bucket',
            object_name='fake_object'
        )

        result = action.run(mock_ctx)

        mocked_conn.return_value.remove_object.assert_called_once_with(
            bucket_name='fake_bucket',
            object_name='fake_object'
        )
        self.assertEqual(True, result)
