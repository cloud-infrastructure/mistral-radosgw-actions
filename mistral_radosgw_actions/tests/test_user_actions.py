from mistral_radosgw_actions import user as actions
from mistral_extra.actions.openstack.utils import keystone
from radosgw.user import UserInfo
from mistral_radosgw_actions.tests import EndpointHelper

from oslotest import base
from unittest import mock

USER = {
    'display_name': 'fake display name',
    'keys': [],
    'caps': [],
    'max_buckets': 1000,
    'swift_keys': [],
    'subusers': [],
    'suspended': 0,
    'user_id': 'fake_user',
    'email': ''
}


class UserActionTest(base.BaseTestCase):

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_user_list_action(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('http://localhost')
        user = UserInfo(radosgw_admin=mocked_conn,
                        user_dict=USER)

        mocked_conn.return_value.get_users.return_value = [user]
        mock_ctx = mock.Mock()

        action = actions.List()

        result = action.run(mock_ctx)

        mocked_conn.return_value.get_users.assert_called_once()
        self.assertIn(user._object, result)

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_user_get_action(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('http://localhost')
        user = UserInfo(radosgw_admin=mocked_conn,
                        user_dict=USER)

        mocked_conn.return_value.get_user.return_value = user
        mock_ctx = mock.Mock()

        action = actions.Get(
            uid=USER['user_id'],
        )

        result = action.run(mock_ctx)

        mocked_conn.return_value.get_user.assert_called_once_with(
            uid=USER['user_id'], stats=False,
        )
        self.assertEqual(USER['user_id'], result['user_id'])

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_user_create_action(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('http://localhost')
        user = UserInfo(radosgw_admin=mocked_conn,
                        user_dict=USER)

        mocked_conn.return_value.create_user.return_value = user
        mock_ctx = mock.Mock()

        action = actions.Create(
            uid=USER['user_id'],
            display_name=USER['display_name'],
            email=USER['email'],
            key_type='s3',
            access_key=None,
            secret_key=None,
            generate_key=False,
            user_caps=USER['caps'],
            max_buckets=USER['max_buckets'],
            suspended=USER['suspended']
        )

        result = action.run(mock_ctx)

        mocked_conn.return_value.create_user.assert_called_once_with(
            uid=USER['user_id'],
            display_name=USER['display_name'],
            email=USER['email'],
            key_type='s3',
            access_key=None,
            secret_key=None,
            generate_key=False,
            user_caps=USER['caps'],
            max_buckets=USER['max_buckets'],
            suspended=USER['suspended']
        )
        self.assertEqual(user._object, result)

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_user_set_action(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('http://localhost')
        user = UserInfo(radosgw_admin=mocked_conn,
                        user_dict=USER)

        mocked_conn.return_value.update_user.return_value = user
        mock_ctx = mock.Mock()

        action = actions.Update(
            uid=USER['user_id'],
            display_name=USER['display_name'],
            email=USER['email'],
            key_type='s3',
            access_key=None,
            secret_key=None,
            generate_key=False,
            user_caps=USER['caps'],
            max_buckets=USER['max_buckets'],
            suspended=USER['suspended']
        )

        result = action.run(mock_ctx)

        mocked_conn.return_value.update_user.assert_called_once_with(
            uid=USER['user_id'],
            display_name=USER['display_name'],
            email=USER['email'],
            key_type='s3',
            access_key=None,
            secret_key=None,
            generate_key=False,
            user_caps=USER['caps'],
            max_buckets=USER['max_buckets'],
            suspended=USER['suspended']
        )
        self.assertEqual(user._object, result)

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_user_delete_action(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('http://localhost')
        mocked_conn.return_value.delete_user.return_value = True
        mock_ctx = mock.Mock()

        action = actions.Delete(
            uid='fake_user',
            purge_data=True
        )

        result = action.run(mock_ctx)

        mocked_conn.return_value.delete_user.assert_called_once_with(
            uid='fake_user',
            purge_data=True
        )
        self.assertEqual(True, result)

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_user_create_key_action(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('http://localhost')
        ACCESS_KEY = 'fake_access_key'
        SECRET_KEY = 'fake_secret_key'
        MOCKED_OUTPUT = [
            {
                'access_key': ACCESS_KEY,
                'secret_key': SECRET_KEY,
                'user': 'fake_user'
            }
        ]

        mocked_conn.return_value.create_key.return_value = MOCKED_OUTPUT
        mock_ctx = mock.Mock()

        action = actions.CreateKey(
            uid=USER['user_id'],
            key_type='s3',
            access_key=ACCESS_KEY,
            secret_key=SECRET_KEY,
            generate_key=False
        )

        result = action.run(mock_ctx)

        mocked_conn.return_value.create_key.assert_called_once_with(
            uid=USER['user_id'],
            key_type='s3',
            access_key=ACCESS_KEY,
            secret_key=SECRET_KEY,
            generate_key=False
        )
        self.assertEqual(MOCKED_OUTPUT, result)

    @mock.patch.object(keystone, 'get_endpoint_for_project')
    @mock.patch('radosgw.connection.RadosGWAdminConnection')
    def test_user_remove_key_action(self, mocked_conn, mocked_ks):
        mocked_ks.return_value = EndpointHelper('http://localhost')
        mocked_conn.return_value.remove_key.return_value = True
        mock_ctx = mock.Mock()

        action = actions.RemoveKey(
            uid='fake_user',
            access_key='fake_access_key',
            key_type='s3'
        )

        result = action.run(mock_ctx)

        mocked_conn.return_value.remove_key.assert_called_once_with(
            uid='fake_user',
            access_key='fake_access_key',
            key_type='s3'
        )
        self.assertEqual(True, result)
