from mistral_radosgw_actions import base


class Get(base.RadosGWAction):
    def __init__(self, uid, quota_type='user',
                 rgw_access=None, rgw_secret=None, action_region=None):
        """Gets the quota of an specific quota_type (user or bucket).
        :param str uid: current user id for the quota operation
        :param str quota_type: type of the quota (user or bucket)
        :param kwargs:
        :return: None
        """
        super(Get, self).__init__(rgw_access, rgw_secret, action_region)
        self.uid = uid
        self.quota_type = quota_type

    def run(self, context):
        return self.get_rgw().get_quota(
            uid=self.uid,
            quota_type=self.quota_type)

    def test(self, context):
        return None


class Set(base.RadosGWAction):
    def __init__(self, uid, quota_type='user', max_objects=None,
                 max_size_kb=None, enabled=None,
                 rgw_access=None, rgw_secret=None, action_region=None):
        """Gets the quota of an specific quota_type (user or bucket).
        :param str uid: current user id for the quota operation
        :param str quota_type: type of the quota (user or bucket)
        :param int max_objects: maximum number of objects
        :param str max_size_kb: quota size in kb
        :param bool enabled: status of the quota config
        :param kwargs:
        :return: None
        """
        super(Set, self).__init__(rgw_access, rgw_secret, action_region)
        self.uid = uid
        self.quota_type = quota_type
        self.max_objects = max_objects
        self.max_size_kb = max_size_kb
        self.enabled = enabled

    def run(self, context):
        return self.get_rgw().set_quota(
            uid=self.uid,
            quota_type=self.quota_type,
            max_objects=self.max_objects,
            max_size_kb=self.max_size_kb,
            enabled=self.enabled
        )

    def test(self, context):
        return None
