from mistral_radosgw_actions import base


class Get(base.RadosGWAction):
    def __init__(self, uid, start, end, show_summary=True, show_entries=True,
                 rgw_access=None, rgw_secret=None, action_region=None):
        super(Get, self).__init__(rgw_access, rgw_secret, action_region)
        self.uid = uid
        self.start = start
        self.end = end
        self.show_summary = show_summary
        self.show_entries = show_entries

    def run(self, context):
        return self.get_rgw().get_usage(
            uid=self.uid,
            start=self.start,
            end=self.end,
            show_summary=self.show_summary,
            show_entries=self.show_entries)

    def test(self, context):
        return None
