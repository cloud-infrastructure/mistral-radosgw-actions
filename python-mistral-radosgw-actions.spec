%global         pypi_name       mistral-radosgw-actions
%global         desc            Action library for operations with Ceph RADOS Gateway

Name:           python-mistral-radosgw-actions
Version:        1.2
Release:        4%{?dist}
Summary:        Python REST API for the Ceph RADOSGW admin operations

License:        ASL 2.0
BuildArch:      noarch
Source0:        %{name}-%{version}.tar.gz

BuildRequires:  python3-setuptools
BuildRequires:  python3-devel

%description
%{desc}

%package -n python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3-setuptools
Requires:       python3-radosgw-admin
Requires:       python3-oslo-log
Requires:       python3-oslo-config
Requires:       python3-mistral
Requires:       python3-mistral-lib
Requires:       python3-mistral-extra

%description -n python3-%{pypi_name}
%{desc}

%prep
%autosetup -p1 -n %{name}-%{version}

%build
%py3_build

%install
%py3_install

%files -n python3-%{pypi_name}
%{python3_sitelib}/mistral_radosgw_actions
%{python3_sitelib}/*.egg-info

%changelog
* Mon Nov 06 2023 Jose Castro Leon <jose.castro.leon@cern.ch> 1.2-4
- Rebuild for yoga

* Wed Apr 19 2023 Jose Castro Leon <jose.castro.leon@cern.ch> 1.2-3
- Rebuild for rhel8 and alma8

* Tue Apr 13 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.2-2
- Initial rebuild for el8s

* Thu Mar 04 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.2-1
- Fix serialization bug after mistral action refactor in victoria

* Thu Jul 30 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1-5
- Fix bug with mistral_extra

* Wed Jun 03 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1-4
- Initial rebuild for el8

* Mon Nov 18 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1-3
- Update base class to simplify region management

* Fri Jan 11 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1-2
- Add actions to manage user access keys

* Fri Oct 26 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1-1
- Fix dependency with issues

* Wed Oct 24 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1-0
- Get radosgw configuration from endpoint using action_region
- Modify actions to use access and secret key from parameters

* Wed Oct 24 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0-4
- Improve handling of operations

* Wed Oct 24 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0-3
- Add quota management entry points
- Add port and is_secure configuration options

* Wed Oct 24 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0-2
- Fix dependency issue on oslo-log

* Mon Oct 22 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0-1
- Update entry_points in setup.cfg

* Wed Oct 17 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0-0
- Initial version of the package
