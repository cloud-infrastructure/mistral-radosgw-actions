# Mistral RadosGW administrative actions

![Build status](https://gitlab.cern.ch/cloud-infrastructure/mistral-radosgw-actions/badges/master/build.svg)
![coverage](https://gitlab.cern.ch/cloud-infrastructure/mistral-radosgw-actions/badges/master/coverage.svg?job=coverage)

### Installation

First of all, clone the repo and go to the repo directory:

  ```.bash
    git clone https://gitlab.cern.ch/cloud-infrastructure/mistral-radosgw-actions.git
    cd mistral-radosgw-actions
  ```

Local installation:


  ```.bash
    pip install mistral-radosgw-actions
  ```

Then we need to tell Mistral about them
and restart Mistral::

  ```.bash
    mistral-db-manage populate;
    systemctl restart openstack-mistral*;
  ```


### Provide radosgw settings properties on your `mistral.conf` file:


```
[radosgw]
signature=<signature>
is_secure=False
```


### Usage


Useful Links
============

* `Mistral`_ <https://github.com/openstack/mistral>
